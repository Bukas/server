﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.IO;
using Core;

namespace AsvApi
{
    public interface IPlugin {
        String Name();
        String Description();
        int Run();
    }

    public abstract class AbstractPlugin : IPlugin {
        protected IAsvServerApi api;

        public AbstractPlugin(IAsvServerApi api) {
            this.api = api;
        }

        public abstract string Description();
        public abstract string Name();
        public abstract int Run();
    }

    public class PluginManager {

		private readonly string PluginDir = "Plugins";

        private List<IPlugin> plugins = new List<IPlugin>();

		public PluginManager(IAsvServerApi api) {
			var pluginDir = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + Path.DirectorySeparatorChar + PluginDir + Path.DirectorySeparatorChar;
			foreach (string name in Directory.GetFiles(pluginDir)) {
				Assembly currentAssembly = null;
				try {
					var aname = AssemblyName.GetAssemblyName(name);
					currentAssembly = Assembly.Load(aname);
					currentAssembly.GetTypes()
						.Where(t => t != typeof(IPlugin) && typeof(IPlugin).IsAssignableFrom(t))
						.ToList()
						.ForEach(x => plugins.Add((IPlugin)Activator.CreateInstance(x, api)));
					#pragma warning disable 168
				} catch (Exception ignored) {
					#pragma warning restore 168
					continue;
				}
			}		
        }

        public IPlugin Load(string name) {
            return null;
        }

        public void Unload(string name) {
        }

        public List<IPlugin> Plugins() {
            return plugins;
        }
    }

    public class AsvApi {
    }
}

