﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Core;

namespace AsvApi
{
    public class InvalidRequestException : Exception {
        public InvalidRequestException() {
        }

        public InvalidRequestException(string message) : base(message) {
        }
    }

	public class ValidationException: Exception {
		
	}

	public class MethodAlreadyExistsException: Exception {
		
	}

	public class NotFoundException: AsvException {
		public NotFoundException(string message) : base(message) {
		}
	}

	public class NotEnoughPermissionsException: Exception {

	}

	public class AsvMethodInfo {
		public AsvPublicApiMethodTemplate Template { get; set; }
		public List<UserRole> Roles;

		public AsvMethodInfo(AsvPublicApiMethodTemplate template) : this(template, null) {
		}

		public AsvMethodInfo(AsvPublicApiMethodTemplate template, List<UserRole> roles) {
			Template = template;
			Roles = roles;
		}
	}

	public class AsvMethodStorage {
		private Dictionary<string, Dictionary<double, AsvMethodInfo>> storage = new Dictionary<string, Dictionary<double, AsvMethodInfo>>();

		public void AddMethod(string name, AsvMethodInfo info) {
			AddMethod(name, 1, info);
		}

		public void AddMethod(string name, float version, AsvMethodInfo info) {
			Dictionary<double, AsvMethodInfo> m = null;
			if (storage.ContainsKey(name)) {
				m = storage[name];
			} else {
				m = new Dictionary<double, AsvMethodInfo>();
				storage.Add (name, m);
			}
			if (m.ContainsKey (version)) {
				throw new MethodAlreadyExistsException ();
			} else {
				m.Add (version, info);
			}
		}

		public AsvMethodInfo Get(string name, double version) {
			try {
				return storage [name] [version];
				#pragma warning disable 168
			} catch (Exception ignored) {
				#pragma warning restore 168
				throw new NotFoundException (AsvException.GetMessage<NotFoundException>(new object[] {name}));
			}
		}
	}

	public delegate byte[] AsvPublicApiMethod(IDataSlice inputData, IAsvServerApi api);

    public class AsvPublicApiMethodTemplate {
        private AsvPublicApiMethod method;

		public AsvPublicApiMethodTemplate(AsvPublicApiMethod method) {
			this.method = method;
        }

        public bool Validate(AsvPublicApiRequest request) {
            return true;
        }

		public byte[] Call(IDataSlice inputData, IAsvServerApi api) {
            return method(inputData, api);
        }
    }

    public class AsvPublicApiRequest
	{
		private DataSlice inputData;
		IAsvServerApi serverApi;
		AsvMethodInfo info;

		public AsvPublicApiRequest(IAsvRequest request, AsvMethodStorage methods, IAsvServerApi serverApi)
		{
			this.serverApi = serverApi;
			string req = System.Text.Encoding.UTF8.GetString(request.Request);
			if (req.Length > 0) {
				if (req [0] == '{') {
					inputData = DataSlice.FromJson (req);
				}
			}
			double version = inputData.Attr ("version").AsDouble;
			if (inputData != null) {
				info = methods.Get(inputData.Attr ("method").Value.ToString(), version);
			}
        }

        public byte[] Call() {
			AsvPublicApiMethodTemplate methodTemplate = info.Template;
			if (!serverApi.CheckUser (serverApi.GetUserBySid (inputData.Attr ("sid").AsString), info.Roles)) {
				throw new NotEnoughPermissionsException ();
			}
            if (methodTemplate.Validate(this)) {
				return methodTemplate.Call(inputData, serverApi);
            } else {
				throw new ValidationException ();
            }
        }
	}
}

