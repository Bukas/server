﻿using System;
using System.Collections.Generic;
using Core;

namespace AsvApi
{
	public interface IAsvServerApi {
		void AddPublicApiMethod(string name, AsvMethodInfo info);
		void AddPublicApiMethod(string name, float version, AsvMethodInfo info);
		User GetUserBySid (string sid);
		bool CheckUser (User user, List<UserRole> roles);
	}

	public interface IAsv {
		void AddContext(IAsvContext context);
		void OfferRequest(IAsvRequest request);
	}

	public interface IAsvContext {
		object OriginalObject { get; }
		IAsvRequest Request { get; }
		IAsvResponse Response { get; set; }
		object Info();
		bool Closed { get; }
	}

	public interface IAsvRequest {
		byte[] Request { get; }
		IAsvContext Context { get; }
	}

    public interface IAsvResponse {
        byte[] Response { set; }
    }

    public interface IAsvListener: IDisposable {
		void Start();
		void Stop();
	}

	public interface IAsvRequestProcessor {
		void ProcessRequest (IAsvRequest request, IAsvServerApi serverApi);
	}
}
