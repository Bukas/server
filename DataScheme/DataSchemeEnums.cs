﻿using System;

namespace DataScheme
{
	public enum EntityType {
		Key,
		Owned,
		Longliving,
		Linking,
		Grouping,
		Fictional
	}

	public enum LinkType {
		OneToOne,
		OneToMany,
		ManyToMany
	}
}

