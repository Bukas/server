﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace AsvConfig
{
	public class IncorrectConfigException: Exception {
	}

	public enum SqlServers {
		MSSQL,
		PostgreSQL
	}

	public enum InstanceRoles {
		Master,
		Licence,
		Query
	}

	public enum Languages {
		Russian,
		English
	}

	public enum Protos {
		Http,
		Https,
		Tcp
	}

	public class Config
	{
		#if DEBUG
		public static string ConfigPath = "..\\config.json";
		#else
		public static string ConfigPath = "config.json";
		#endif

		private SqlServers sqlServer;
		private Languages language;
		private List<InstanceRoles> instanceRoles = new List<InstanceRoles>();
		private List<Protos> protocols = new List<Protos> ();

		public string Language { 
			get { return language.ToString (); } 
			set { language = (Languages) Enum.Parse (typeof(Languages), value); }
		}

		public string SqlServer { 
			get { return sqlServer.ToString (); } 
			set { sqlServer = (SqlServers) Enum.Parse (typeof(SqlServers), value); }
		}

		public string MasterDatabase { get; set; }

		public string[] Roles {
			get { 
				return new List<string> (
					instanceRoles.ConvertAll(
						new Converter<InstanceRoles, string>( (ir) => ir.ToString() )
					)
				).ToArray(); 
			}
			set {
				instanceRoles = new List<InstanceRoles> (
					new List<string>(value).ConvertAll(
						new Converter<string, InstanceRoles>( (s) => (InstanceRoles) Enum.Parse(typeof(InstanceRoles), s) )
					)
				); 
			}
		}

		public string[] Protocols {
			get { 
				return new List<string> (
					protocols.ConvertAll(
						new Converter<Protos, string>( (proto) => proto.ToString() )
					)
				).ToArray(); 
			}
			set {
				protocols = new List<Protos> (
					new List<string>(value).ConvertAll(
						new Converter<string, Protos>( (s) => (Protos) Enum.Parse(typeof(Protos), s) )
					)
				); 
				if (protocols.Count == 0) {
					protocols = GetDefaultProtocols ();
				}
			}
		}

		public string MasterServer { get; set; }

		public int Port { get; set; }

		public int MinimumThreads { get; set; }
		public int MaximumThreads { get; set; }

		protected List<Protos> GetDefaultProtocols() {
			Protos[] p = { Protos.Http };
			return new List<Protos>(p);
		}

		public static void Save(Config config) {
			string json = JsonConvert.SerializeObject(config);
			File.WriteAllText (ConfigPath, json);
		}

		public static Config Load() {
			string json = File.ReadAllText (ConfigPath);
			Config c = JsonConvert.DeserializeObject<Config> (json);

			if (c.Language == String.Empty)
				c.Language = Languages.Russian.ToString();

			if ((new List<string> (c.Roles).Contains (InstanceRoles.Master.ToString ())) && (c.MasterDatabase == String.Empty))
				throw new IncorrectConfigException ();
			if ((! (new List<string> (c.Roles).Contains (InstanceRoles.Master.ToString ()))) && (c.MasterServer == String.Empty))
				throw new IncorrectConfigException ();

			if (c.protocols.Count == 0)
				c.protocols = c.GetDefaultProtocols ();

			if (c.Port == 0)
				c.Port = 16853;

			if (c.MinimumThreads == 0)
				c.MinimumThreads = 10;
			if (c.MaximumThreads < c.MinimumThreads)
				c.MaximumThreads = c.MinimumThreads + 5;

			return c;
		}

		public Config Clone() {
			Config c = new Config ();
			c.MasterDatabase = MasterDatabase;
			c.MasterServer = MasterServer;
			c.SqlServer = SqlServer;
			c.Roles = new List<string> (Roles).ToArray ();
			return c;
		}
	}
}

