﻿using NUnit.Framework;
using System;
using Core;
using System.Globalization;

namespace AsvTest
{
	[TestFixture ()]
	public class CoreClassesTest
	{
		private class Checker: IValueChecker {
			public bool CheckValue (Object Value) {
				return ((int) Value == 5);
			}
		}

		[Test ()]
		public void DataSliceAttrCreate () {
			DataSliceAttr<int> f = new DataSliceAttr<int> ("myint");
			Assert.AreEqual ("myint", f.Name);
		}

		[Test ()]
		public void DataSliceAttrAssign() {
			DataSliceAttr<int> f = new DataSliceAttr<int> ("myint");
			f.Value = 1;
			int i = f.Value;
			Assert.AreEqual (1, i);
		}

		[Test ()]
		 public void DataSliceAttrExternalChecker() {
			Checker c = new Checker();
			DataSliceAttr<int> f = new DataSliceAttr<int> ("myint");
			f.ValueChecker = c;
			Assert.Throws<System.Exception>(delegate { f.Value = 1; });
		}

		[Test ()]
		public void DataSliceAttrHistoric() {
			HistoryValue[] hv = new HistoryValue[2];
			hv [0] = new HistoryValue (DateTime.ParseExact("20150201000000", "yyyyMMddHHmmss", CultureInfo.InvariantCulture), 2); //01.02.2015
			hv [1] = new HistoryValue (DateTime.ParseExact("20150301000000", "yyyyMMddHHmmss", CultureInfo.InvariantCulture), 3); //01.03.2015
			DataSliceAttrHistoric a = new DataSliceAttrHistoric ("myHist", hv); 
			a.Add (DateTime.ParseExact("20150101000000", "yyyyMMddHHmmss", CultureInfo.InvariantCulture), 1);                     //01.01.2015

			Assert.AreEqual(a.Value(DateTime.Now), 3);
			Assert.AreEqual (a.Value (DateTime.ParseExact ("20140301000000", "yyyyMMddHHmmss", CultureInfo.InvariantCulture)), null); //01.03.2014
			Assert.AreEqual (a.Value (DateTime.ParseExact ("20150120000000", "yyyyMMddHHmmss", CultureInfo.InvariantCulture)), 1);    //20.01.2015
		}


		[Test ()]
		public void DataSliceRecord() {
			DataSliceRecord r = new DataSliceRecord (new[] {new DataSliceAttr<int>("int1"), new DataSliceAttr<int>("int2")});
			r.Attr("int1").Value = 3;
			Assert.AreEqual (r.Attr ("int1").Value, 3);
		}

		[Test ()]
		public void DataSliceTest() {
			DataSlice ds = new DataSlice ();
			ds.Add (new DataSliceRecord (new[] { ((DataSliceAttr)new DataSliceAttr<string> ("str")), ((DataSliceAttr)new DataSliceAttrHistoric<int> ("hi")) } ));
			ds.Add (new DataSliceRecord (new[] { ((DataSliceAttr)new DataSliceAttr<string> ("str")), ((DataSliceAttr)new DataSliceAttrHistoric<int> ("hi")) } ));
			Assert.AreEqual (ds.Current, 1);
			Assert.AreEqual (ds.Count, 2);

			ds.First ();
			ds.Attr ("str").Value = "one";
			ds.Next ();
			ds.Attr ("str").Value = "two";
			ds.Last ();
			Assert.AreEqual (ds.Value ("str"), "two");
			ds.Previous ();
			Assert.AreEqual (ds.Value ("str"), "one");
			ds.Locate ("str", "two");
			Assert.AreEqual (ds.Value ("str"), "two");
			Assert.AreEqual (ds.Current, 1);
			string s = "";
			foreach (DataSliceRecord dsr in ds)
				s = s + dsr.Value ("str");
			Assert.IsTrue (s.Equals ("onetwo"));
		}

		[Test ()]
		public void FromJson() {
			string json = "{\"strvar\":\"asd\"}";
			DataSlice ds = DataSlice.FromJson(json);
			Assert.AreEqual(ds.Attr("strvar").Value, "asd");
		}
	}
}

