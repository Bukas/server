﻿using System.Collections.Generic;
using NUnit.Framework;
using System;
using AsvConfig;
using Localization;

namespace AsvTest
{
	[TestFixture ()]
	public class AsvConfigTest
	{
		[Test]
		public void ConfigLoad () {
			Config c = Config.Load ();
			Assert.AreEqual (c.SqlServer, "PostgreSQL");
			Assert.AreEqual (c.MasterDatabase, "http://localhost:5432");
			//Assert.IsTrue (c.Roles.Contains ("Master"));
		}

		[Test]
		public void ConfigSave () {
			Config c = new Config ();
			c.SqlServer = "PostgreSQL";
			c.MasterDatabase = "http://localhost:5432";
			c.Roles = new [] { InstanceRoles.Master.ToString(), InstanceRoles.Query.ToString() };
			Config.Save (c);
		}

		[Test]
		public void LocalizationLoad() {
			var lc = new LocalizedConst(Languages.Russian.ToString());
			Assert.AreEqual (lc.Get("NotFoundException"), "Не найдено: {0}");
		}
	}
}

