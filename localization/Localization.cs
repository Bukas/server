﻿using System;
using System.IO;
using System.Collections.Generic;
using Ionic.Zip;
using Newtonsoft.Json;

namespace Localization
{
	public class LocalizedConst
	{
		#if DEBUG
		public static string LocalizationPath = "..\\localization.aac";
		#else
		public static string LocalizationPath = "localization.aac";
		#endif

		private Dictionary<string, string> constStorage = new Dictionary<string, string>();
			
		public LocalizedConst(string lang) {
			string locPath = lang;
			using (ZipFile zip = ZipFile.Read (LocalizationPath)) {
				ZipEntry e = zip[locPath];
				using (var stream = new MemoryStream()) {
					e.Extract(stream);
					stream.Position = 0;
					using (var reader = new StreamReader (stream)) {
						string loc = reader.ReadToEnd ();
						constStorage = JsonConvert.DeserializeObject<Dictionary<string, string>>(loc);
					}
				}
			}
		}

		public string Get(string key) {
			if (constStorage.ContainsKey (key)) {
				return constStorage [key];
			} else {
				return String.Empty;
			}
		}
	}
}

