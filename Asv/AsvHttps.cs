﻿using System;
using AsvApi;

namespace Asv
{
	public class AsvHttpSecureListener: AsvHttpListener
	{
		public AsvHttpSecureListener (int port, IAsv asv): base(port, asv)
		{
			listener.Prefixes.Clear();
			string url = String.Format ("https://*:{0}/", port);
			listener.Prefixes.Add(url);
		}
	}
}

