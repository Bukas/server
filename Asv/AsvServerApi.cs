﻿using System;
using AsvApi;
using Core;
using System.Collections.Generic;

namespace Asv
{
	public class AsvServerApi: IAsvServerApi {
		private AppServer asv;

		public AsvServerApi(AppServer asv) {
			this.asv = asv;
		}

		public void AddPublicApiMethod(string name, AsvMethodInfo info) {
			asv.publicApiMethods.AddMethod (name, info);
		}

		public void AddPublicApiMethod(string name, float version, AsvMethodInfo info) {
			asv.publicApiMethods.AddMethod (name, version, info);
		}

		public User GetUserBySid(string sid) {
			return asv.GetUserBySid(sid);
		}

		public bool CheckUser(User user, List<UserRole> roles) {
			if (roles == null)
				roles = new List<UserRole> ();
			if (roles.Count == 0) {
				roles.Add (new UserRole("public"));
			}

			if (user == null)
				user = new User ();

			bool check = false;
			foreach (UserRole role in roles) {
				if (user.roles.Contains (role))
					check = true;
			}
			return check;
		}
	}
}