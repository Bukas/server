﻿using System;

namespace Asv
{
	public sealed class AsvGlobal
	{
		private static volatile AsvGlobal instance;
		private static object syncRoot = new Object();

		private AsvGlobal() {}

		public static AsvGlobal Instance {
			get  {
				if (instance == null)  {
					lock (syncRoot)  {
						if (instance == null) 
							instance = new AsvGlobal();
					}
				}
				return instance;
			}
		}
	}
}

