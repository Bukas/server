﻿using System;
using AsvConfig;
using Localization;
using Core;

namespace Asv
{
	public class Asv
	{
		static void Main() 
		{
			var config = Config.Load ();
			Globals.Init (config);
			var appServer = new AppServer(config);
			appServer.Start();
		}
	}
}
