﻿using System;
using AsvConfig;
using Localization;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Threading;
using System.Text;
using AsvApi;
using Core;

namespace Asv
{
	public class DefaultProcessor: IAsvRequestProcessor {
        AppServer asv;
		IAsvServerApi serverApi;

		public DefaultProcessor(AppServer asv, IAsvServerApi serverApi) {
            this.asv = asv;
			this.serverApi = serverApi;
        }

		public void ProcessRequest (IAsvRequest request, IAsvServerApi serverApi) {
			AsvPublicApiRequest paRequest = new AsvPublicApiRequest(request, asv.publicApiMethods, serverApi);
			request.Context.Response.Response = paRequest.Call();
		}
	}

	public class AppServer: IAsv
	{
		private ConcurrentQueue<IAsvRequest> requestQueue = new ConcurrentQueue<IAsvRequest>();
		private List<IAsvListener> listeners = new List<IAsvListener> ();
		private List<Thread> workers = new List<Thread> ();
		private static readonly object monitor = new object();
		private bool isRunning = false;
		private IAsvRequestProcessor processor;
        private PluginManager pluginManager;
		public AsvMethodStorage publicApiMethods = new AsvMethodStorage();
		private IAsvServerApi serverApi;

		public AppServer (Config config)
		{
			serverApi = new AsvServerApi (this);
			pluginManager = new PluginManager(serverApi);
			foreach (IPlugin plugin in pluginManager.Plugins()) {
				plugin.Run ();
			}
			foreach (string sp in config.Protocols) {
				Protos p = (Protos) Enum.Parse (typeof(Protos), sp);
				IAsvListener listener = null;
				switch (p) {
				case Protos.Http:
					listener = new AsvHttpListener (config.Port, this);
					break;
				case Protos.Https:
					listener = new AsvHttpSecureListener(config.Port, this);
					break;
				}
				if (listener != null) {
					listeners.Add (listener);
				}
			}
			for (int i = 0; i < 10; i++) {
				workers.Add (new Thread (new ThreadStart (WorkerProc)));
			}
			processor = new DefaultProcessor (this, serverApi);
		}

		public void Start() {
			isRunning = true;
			foreach (IAsvListener listener in listeners) {
				listener.Start();
			}
			foreach (Thread w in workers) {
				w.Start ();
			}
		}

		public void Stop() {
			isRunning = false;
			foreach (IAsvListener listener in listeners) {
				listener.Stop();
			}
			foreach (Thread w in workers) {
				w.Join ();
			}

			isRunning = false;
		}

		public void AddContext(IAsvContext context) {
			
		}

		public void OfferRequest(IAsvRequest request) {
			lock (monitor) {
				requestQueue.Enqueue (request);
				Monitor.Pulse (monitor);
			}
		}

		private String WrapException(Exception e) {
			DataSlice dse = new DataSlice ();
			dse.Add(new DataSliceRecord(new [] {
				new DataSliceAttr("result", "error"),
				new DataSliceAttr("code", e.ExceptionCode()),
				#if DEBUG
				new DataSliceAttr("message", e.FullExceptionMessage())
				#else
				new DataSliceAttr("message", e.PublicExceptionMessage())
				#endif
			}));
			return dse.ToJson ();
		}

		private void WorkerProc() {
			while (isRunning) {
				var a = new List<int> ();
				IAsvRequest request;
				lock (monitor) {
					if (requestQueue.Count == 0) {
						Monitor.Wait (monitor);
					}
				}
				requestQueue.TryDequeue (out request);
				lock (request) {
					if (request != null && (!request.Context.Closed)) {
						try {
							processor.ProcessRequest (request, serverApi);
						} catch (Exception e) {
							request.Context.Response.Response =  Encoding.UTF8.GetBytes(WrapException(e));
						}
					}
				}
			}
		}

		public User GetUserBySid(string sid) {
			//todo
			return null;
		}
	}
}

