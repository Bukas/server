﻿using System;
using System.Net;
using System.Web;
using System.Threading;
using System.IO;
using AsvApi;

namespace Asv
{
    public class AsvHttpContext : IAsvContext
    {
        private HttpListenerContext context;
        private AsvHttpResponse response;

        public AsvHttpContext(HttpListenerContext context) {
            this.context = context;
            response = new AsvHttpResponse(this);
        }

        public bool Closed {
            get {
				return false;
            }
        }

        public object OriginalObject {
            get {
                return context;
            }
        }

        public IAsvResponse Response
        {
            get {
                return response;
            }

            set  {
                response = (AsvHttpResponse) value;
            }
        }

        public object Info()
        {
            throw new NotImplementedException();
        }

		public IAsvRequest Request {
			get { 
				return new AsvHttpRequest (this);
			}
        }
    }

    public class AsvHttpRequest : IAsvRequest {
        private AsvHttpContext context;
        private HttpListenerRequest request;
		private byte[] requestData;

		public void SetRequestData(byte[] data) {
			requestData = data;
		}

        public AsvHttpRequest(AsvHttpContext context) {
            this.context = context;
            request = ((HttpListenerContext)context.OriginalObject).Request;
        }

        public IAsvContext Context {
            get {
                return context;
            }
        }

        public byte[] Request {
            get {
				if (requestData != null) {
					return requestData;
				} else {
					using (MemoryStream ms = new MemoryStream ()) {
						request.InputStream.CopyTo (ms);
						return ms.ToArray ();
					}
				}
            }
        }

        public HttpListenerRequest OriginalObject {
            get {
                return request;
            }
        }
    }

    public class AsvHttpResponse : IAsvResponse
    {
        private AsvHttpContext context;

        public AsvHttpResponse(AsvHttpContext context) {
            this.context = context;
        }

        public AsvHttpResponse(AsvHttpRequest request) {
            this.context = (AsvHttpContext) request.Context;
        }

        public byte[] Response {
            set {
                HttpListenerResponse response = ((HttpListenerContext)context.OriginalObject).Response;
				response.ContentEncoding = System.Text.Encoding.UTF8;
                response.OutputStream.Write(value, 0, value.Length);
                response.OutputStream.Close();
            }
        }
    }

    public class AsvHttpListener: IAsvListener
	{
		protected readonly HttpListener listener;
		private Thread listeningThread;
		private IAsv asv;
		private volatile bool isListening = false;

		public AsvHttpListener (int port, IAsv asv) {
			this.asv = asv;
			string url = String.Format ("http://*:{0}/", port);
			listener = new HttpListener ();
			listener.Prefixes.Add(url);
			listeningThread = new Thread (new ThreadStart (Run));
		}

		public void Start() {
			listener.Start();
			listeningThread.Start ();
			isListening = true;
		}

		public void Stop() {
			Dispose ();
		}

		public void Run() {
			while (isListening) {
                AsvHttpRequest request = Listen();
                request = preprocessRequest(request);
				asv.OfferRequest(request);
			}
		}

		public void Dispose() {
			isListening = false;
			listener.Stop ();
			listeningThread.Join ();
		}

		public AsvHttpRequest Listen() {
			var hlc = listener.GetContext ();
            var context = new AsvHttpContext(hlc);
            return new AsvHttpRequest(context);
		}

        private AsvHttpRequest preprocessRequest(AsvHttpRequest request) {
            HttpListenerRequest original = request.OriginalObject;
			if (original.HttpMethod == "GET") {
				Uri uri = new Uri (original.Url.AbsoluteUri);
				string url = WebUtility.UrlDecode (uri.AbsoluteUri);
				string path = uri.AbsolutePath;
				if ((path.Length > 1) && (path [0] == '/')) {
					path = path.Substring(1);
				}
				string method = null;
				if (path.Equals ("/"))
					method = "test";
				else
					method = path;
				string json = String.Empty;
				var p = HttpUtility.ParseQueryString (uri.Query);
				json += "\"method\": " + string.Format("\"{0}\"", method) + ',';
				if (! Array.Exists(p.AllKeys, element => element == "version"))
					json += "\"version\": 1,";
				foreach (string key in p.AllKeys) {
					int n;
					float f;
					if (int.TryParse (p [key], out n)) {
						json += string.Format ("\"{0}\"", key) + ':' + p [key] + ',';
					} else if (float.TryParse (p [key].Replace (',', '.'), out f)) {
						json += string.Format ("\"{0}\"", key) + ':' + p [key].Replace (',', '.') + ',';
					} else {
						json += string.Format ("\"{0}\"", key) + ':' + string.Format ("\"{0}\"", p [key]) + ',';
					}
				}
				json = json.Substring (0, json.Length - 1);
				json = "{" + json + "}";
				request.SetRequestData (System.Text.Encoding.UTF8.GetBytes(json));
            }
            return request;
        }
    }
}