﻿using System;
using Localization;
using AsvConfig;

namespace Core
{
	public static class Globals
	{
		private static LocalizedConst localConst;

		public static void Init(Config config) {
			localConst = new LocalizedConst (config.Language);
		}

		public static string GetString(string name) {
			return localConst.Get (name);
		}

		public static string GetString(string name, object[] objects) {
			string s = localConst.Get (name);
			if (objects != null) {
				for (int i = 0; i < objects.Length; i++) {
					s = s.Replace ("{" + i.ToString () + "}", objects [i].ToString ());
				}
			}
			return s;
		}

		public static string FullExceptionMessage (this Exception e) {
			string msg = e.Message;
			if (e.InnerException != null) {
				msg += "; caused by: " + e.InnerException.FullExceptionMessage();
			}
			return msg;
		}

		public static string PublicExceptionMessage (this Exception e) {
			//todo
			return e.Message;
		}

		public static int ExceptionCode (this Exception e) {
			//todo
			return 1;
		}
	}
}

