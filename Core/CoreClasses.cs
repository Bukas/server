﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Text;

namespace Core
{
	public class AsvSerializer 
	{
		public static string SerializeToJson(object o, Encoding enc) {
			var d = new DataContractJsonSerializer(o.GetType());
			var ms = new MemoryStream();
			d.WriteObject(ms, o);
			return enc.GetString(ms.ToArray()).Replace("\\/", "/");
		}

		public static string SerializeToJson(object o) {
			return SerializeToJson(o, Encoding.UTF8);
		}

		public static T DeserializeFromJson<T>(string json, Encoding enc) {
			var d = new DataContractJsonSerializer(typeof(T));
			var ms = new MemoryStream(enc.GetBytes(json));
			return (T) d.ReadObject(ms);
		}

		public static T DeserializeFromJson<T>(string json) {
			return DeserializeFromJson<T>(json, Encoding.UTF8);
		}

		public static string FormatJson(string json) {
			int t = 0;
			int i = 0;
			string subst;
			string specialLetters = "{}[],";
			string openLetters = "{[";
			string closeLetters = "}]";
			while (i < json.Length) {
				if (specialLetters.Contains(json[i].ToString())) {
					if (closeLetters.Contains(json[i].ToString()))
						t--;
					subst = new String('\t', t);
					if (openLetters.Contains(json[i].ToString())) 
						t++;
					if (((openLetters.Contains(json[i].ToString())) || (closeLetters.Contains(json[i].ToString()))) && (i > 0)) {
						subst = "\n" + subst;
						json = json.Insert(i, subst);
						i = i + subst.Length;
					}
					if (i < json.Length - 1) {
						subst = "\n" + (new String('\t', t));
						json = json.Insert(i + 1, subst);
						i = i + subst.Length;
					}
					if (i > 0)
						json = json.Substring(0, i + 1) + json.Substring(i + 1, json.Length - i - 1).Trim();
				}
				i++;
			}
			return json;
		}
	}

	public class ClassBuilder
	{
		
	}

	public interface IValueChecker
	{
		bool CheckValue (Object Value);
	}

	public class HistoryValue
	{
		public DateTime Since;
		public object Value;

		public HistoryValue(DateTime since, object value) {
			Since = since;
			Value = value;
		} 
	}

    public class InvalidAttrValueException : Exception
    {
        public InvalidAttrValueException(string message): base(message) { 
        }
    }

    public class DataSliceAttr
	{
		protected object v;
		protected string name;
		protected bool isOptional = false;

		public DataSliceAttr(string attrName) {
			name = attrName;
		}

		public DataSliceAttr(string attrName, object value) {
			name = attrName;
			v = value;
		}

		public virtual bool CheckValue(Object value) {
			return true;
		}

		public object Value { 
			get { return v;	}
			set { 
				if ((CheckValue (value)) && ((ValueChecker == null) || (ValueChecker.CheckValue(value))))
					v = value;
				else
					throw new InvalidAttrValueException(name);			
			}
		}

		public string Name {
			get { return name; }
		}

		public IValueChecker ValueChecker {
			get;
			set;
		}

		public bool IsOptional {
			get { return isOptional; }
		}

		public string AsString {
			get { 
				if (v != null)
					return v.ToString ();
				else
					return null;
			}
		}

		public override string ToString() {
			return AsString;
		}

		public int AsInt {
			get {return Convert.ToInt32(v); }
		}

		public double AsDouble {
			get {return Convert.ToDouble(v); }
		}

		public bool IsNumber()
		{
			if (v == null)
				return false;
			return v is sbyte
				|| v is byte
				|| v is short
				|| v is ushort
				|| v is int
				|| v is uint
				|| v is long
				|| v is ulong
				|| v is float
				|| v is double
				|| v is decimal;
		}

		public string ToJson() {
			StringBuilder sb = new StringBuilder (String.Empty);
			sb.Append ("\"");
			sb.Append (name);
			sb.Append ("\": ");
			bool isNum = IsNumber ();
			if (!isNum)
				sb.Append ("\"");
			sb.Append (this.ToString ());
			if (!isNum)
				sb.Append ("\"");
			return sb.ToString ();
		}
	}

	public class DataSliceAttr<T> : DataSliceAttr
	{
		public DataSliceAttr(string attrName): base(attrName) {}

		public new T Value {
			get { return (T) v; }
			set { base.Value = value; }
		}
	}

	public class DataSliceAttrHistoric: DataSliceAttr
	{
		protected SortedList histValues;

		public DataSliceAttrHistoric (string attrName): base(attrName) {
			histValues = new SortedList();
		}

		public DataSliceAttrHistoric (string attrName, HistoryValue[] hv): base(attrName) {
			histValues = new SortedList();
			foreach (HistoryValue v in hv)
				histValues.Add (v.Since, v.Value);
		}

		public new object Value(DateTime dateTime) {
			if ( DateTime.Compare(dateTime, ((DateTime)histValues.GetKey (0))) < 0)
				return null;
			for (int i = 0; i < histValues.Count - 1; i++) {
				if (DateTime.Compare(((DateTime)histValues.GetKey (i + 1)), dateTime) > 0)
					return histValues.GetByIndex (i);
			}
			return histValues.GetByIndex (histValues.Count - 1);
		}

		public new object Value() {
			return Value (DateTime.Now);
		}

		public void Add(DateTime since, object value) {
			histValues.Add (since, value);
		}

		public void Add(HistoryValue hValue) {
			histValues.Add (hValue.Since, hValue.Value);
		}
	}

	public class DataSliceAttrHistoric<T> : DataSliceAttrHistoric
	{
		public DataSliceAttrHistoric(string attrName): base(attrName) {}

		public DataSliceAttrHistoric (string attrName, HistoryValue[] hv): base(attrName, hv) {}

		public new T Value (DateTime dateTime) {
			return (T) base.Value(dateTime);
		}
	}

	public class DataSliceRecord
	{
		private Dictionary<String, DataSliceAttr> attrs = new Dictionary<String, DataSliceAttr>();

		public DataSliceRecord() { 
		}

		public DataSliceRecord(DataSliceAttr[] fields): this()
		{
			foreach (DataSliceAttr field in fields)
			{
				attrs.Add(field.Name, field);
			}
		}

		public List<DataSliceAttr> AttrList
		{
			get { return new List<DataSliceAttr>(attrs.Values); }
		}

		public DataSliceAttr Attr(string attrName, object def = null)
		{
			if (attrs.ContainsKey (attrName))
				return attrs [attrName];
			else
				return new DataSliceAttr(attrName, def);
		}

		public bool ContainsAttr(string attrName) {
			return attrs.ContainsKey(attrName);
		}

		public void Add(DataSliceAttr field) {
			attrs.Add(field.Name, field);
		}

		public object Value(string attrName)
		{
			return attrs[attrName].Value;
		}

		public object Value(string attrName, DateTime dateTime)
		{
			DataSliceAttr f = Attr(attrName);
			if (f is DataSliceAttrHistoric)
				return ((DataSliceAttrHistoric)f).Value(dateTime);
			else
				return f.Value;
		}

		public string ToJson() {
			StringBuilder sb = new StringBuilder (String.Empty);
			sb.Append ("{");
			List<DataSliceAttr> a = attrs.Values.ToList ();
			for (int i = 0; i < a.Count; i++) {
				sb.Append (a[i].ToJson());
				if (i < attrs.Count - 1)
					sb.Append (",");
			}
			sb.Append ("}");
			return sb.ToString ();
		}
	}


	public interface IDataSlice
	{
		bool Next ();
		bool Previous ();
		bool Eof();
		void First();
		void Last();
		int Count { get; }
		int Current { get; set; }
		void Add (DataSliceRecord dataSliceRecord);
		void Delete ();
		void Delete (int index);
		DataSliceRecord Record (int index);
		IDataSlice ChildSlice (string sliceName);
		DataSliceAttr Attr (string attrName, object def = null);
		object Value (string attrName);
		object Value (string attrName, DateTime dateTime);
		void Locate (string attrName, object attrValue);
	}

	public class DataSlice: IDataSlice, ICloneable, IEnumerable
	{
		private class DataSliceEnumerator: IEnumerator
		{
			private int pos = -1;
			private List<DataSliceRecord> recs;

			public DataSliceEnumerator(List<DataSliceRecord> r)
			{
				recs = r;
			}

			public bool MoveNext()
			{
				pos++;
				return pos < recs.Count;
			}

			public void Reset()
			{
				pos = -1;
			}

			public object Current
			{
				get
				{
					try
					{
						return recs[pos];
					}
					catch(IndexOutOfRangeException) 
					{
						throw new InvalidOperationException ();
					}
				}
			}
		}

		private List<DataSliceRecord> recs = new List<DataSliceRecord>();
		private int current = 0;

		public bool Next() {
			if (current < recs.Count - 1) {
				current++;
				return true;
			} else
				return false;
		}

		public bool Previous() {
			if (current > 0) {
				current--;
				return true;
			} else
				return false;
		}

		public bool Eof() {
			return ( current < recs.Count -1);
		}

		public void First() {
			current = 0;
		}

		public void Last() {
			current = recs.Count - 1;
		}

		public int Count {
			get { return recs.Count; }
		}

		public int Current {
			get { 
				return current; 
			}
			set { 
				if ((value >= 0) && (value < recs.Count))
					current = value;
			}
		}

		public void Add(DataSliceRecord Record) {
			recs.Add (Record);
			current = recs.Count - 1;
		}

		public void Delete () {
			recs.RemoveAt (current);
		}

		public void Delete(int index) {
			recs.RemoveAt (index);
		}

		public DataSliceRecord Record(int index) {
			return recs [index];
		}

		public IDataSlice ChildSlice (string sliceName) {
			return (IDataSlice)Value (sliceName);
		}

		public DataSliceAttr Attr(string attrName, object def = null) {
			return recs[current].Attr(attrName, def);
		}

		public object Value(string attrName) {
			return recs [current].Value (attrName);
		}

		public object Value(string attrName, DateTime dateTime) {
			return recs [current].Value (attrName, dateTime);
		}

		public void Locate(string attrName, object attrValue) {
			for (int i = 0; i < recs.Count; i++)
				if (recs[i].Value (attrName).Equals (attrValue)) {
					current = i;
					return;
				}
		}

		public Object Clone()
		{
			return null;  //todo
		}

		public IEnumerator GetEnumerator()
		{
			return new DataSliceEnumerator(recs);
		}

		private static DataSliceAttr ValueFromToken(JToken token, string attrName)
		{
			DataSliceAttr a = new DataSliceAttr (attrName);
			object o = null;
			if (token is JValue) {
				o = ((JValue)token).Value;
			} else if (token is JArray) {
				JArray arr = (JArray)token;
				DataSlice ds = new DataSlice();
				foreach (JToken t in arr) {
					ds.Add(RecFromToken(t));
				}
				o = ds;
			} else if (token is JContainer) {
				DataSlice ds = new DataSlice();
				ds.Add(RecFromToken(token));
				o = ds;
			}
			a.Value = o;
			return a;
		}

		private static DataSliceRecord RecFromToken(JToken token)
		{
			DataSliceRecord rec = new DataSliceRecord();
			if (token is JProperty) {
				JProperty prop = (JProperty)token;
				rec.Add(ValueFromToken(prop.Value, prop.Name));
			} else if (token is JArray) {
				JArray arr = (JArray)token;
				DataSlice ds = new DataSlice();
				foreach (JToken t in arr) {
					ds.Add(RecFromToken(t));
				}
				string s = Guid.NewGuid().ToString();
				rec.Add(new DataSliceAttr(s));
				rec.Attr(s).Value = ds;
			} else if (token is JContainer) {
				foreach (JToken t in token.Children()) {
					if (t is JProperty) {
						var prop = (JProperty)t;
						rec.Add(ValueFromToken(prop.Value, prop.Name));
					} else if (t is JContainer) {
						string s = Guid.NewGuid().ToString();
						rec.Add(ValueFromToken(t, s));
					}
				}
			}
			return rec;
		}

		public static DataSlice FromJson(string json) {
			DataSlice ds = new DataSlice();
			JToken jt = JToken.Parse(json);
			ds.Add(RecFromToken(jt));
			return ds;
		}

		public string ToJson() {
			StringBuilder sb = new StringBuilder (String.Empty);
			for (int i = 0; i < recs.Count; i++) {
				sb.Append (recs[i].ToJson ());
				if (i < recs.Count - 1)
					sb.Append (",");
			}
			if (recs.Count > 1) {
				sb.Insert (0, "[");
				sb.Append ("]");
			}
			return JObject.Parse(sb.ToString ()).ToString();
		}

		public void Validate(DataSlice ds) {
			if (recs.Count > 0) {
				if (ds.recs.Count == 0) {
					throw new Exception();
				}
				foreach (DataSliceRecord rec in ds) {
					for (int i = 0; i < recs[0].AttrList.Count; i++) {
						bool contains = rec.ContainsAttr(recs[0].AttrList[i].Name);
						if ((!contains) && (!recs[0].AttrList[i].IsOptional)) {
							throw new Exception();
						} else if (contains) {
							DataSliceAttr attr = rec.Attr(recs[0].AttrList[i].Name);
							if (attr.Value is DataSlice) {
								((DataSlice) recs[0].AttrList[i].Value).Validate((DataSlice) attr.Value);
							} else {
								if (!recs[0].AttrList[i].CheckValue(attr.Value)) {
									throw new Exception(recs[0].AttrList[i].Name);
								}
							}
						}
					}
				}
			}
		}
	}

	public interface DataProcessor
	{
		DataSlice Process(UserRole role, DataSlice ds);
	}

	public sealed class Persistent
	{
		private static volatile Persistent instance;
		private static object syncRoot = new Object();

		private Persistent() {}

		public static Persistent Instance {
			get {
				if (instance == null) {
					lock (syncRoot) {
						if (instance == null) 
							instance = new Persistent();
					}
				}
				return instance;
			}
		}
	}
}

