﻿using System;
using Localization;

namespace Core
{
	public class AsvException : Exception
	{
		public AsvException() : base() {}

		public AsvException(string message) : base(message) {
		} 

		public AsvException(Exception innerEx) {
		} 

		public AsvException(string message, Exception innerEx) : base(message, innerEx) {
		}

		public static string GetMessage(string name) {
			return Globals.GetString(name);
		}

		public static string GetMessage(string name, object[] objects) {
			return Globals.GetString(name, objects);
		}

		public static string GetMessage<T>() {
			return Globals.GetString(typeof(T).Name);
		}

		public static string GetMessage<T>(object[] objects) {
			return Globals.GetString(typeof(T).Name, objects);
		}

		/*public static void Throw<T>(object[] objects, Exception innerEx) where T:AsvException, new() {
			throw new T (GetMessage (typeof(T).Name, objects), innerEx);
		}

		public static void Throw<T>(object[] objects) where T:AsvException {
			throw new T (GetMessage (typeof(T).Name, objects));
		}

		public static void Throw<T>() where T:AsvException {
			throw new T (GetMessage (typeof(T).Name));
		}*/
	}
}

