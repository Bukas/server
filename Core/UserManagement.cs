﻿using System;
using System.Collections.Generic;

namespace Core
{
	public class User {
		private string name;
		public List<UserRole> roles = new List<UserRole>();

		public string Name {
			get {
				return name;
			}
			set {
				name = value;
			}
		}

		public User() {
			roles.Add (new UserRole ("public"));
		}
	}

	public class UserRole
	{
		private string name;

		public string Name {
			get {
				return name;
			}
			set {
				name = value;
			}
		}

		public UserRole(string name) {
			this.name = name;
		}

		public override bool Equals(object role) {
			return Name.Equals (((UserRole) role).Name);
		}

		public override int GetHashCode() {
			return Name.GetHashCode ();
		}
	}

	public class UserRoleSet {
		public Dictionary<string, UserRole> roles = new Dictionary<string, UserRole>();
	}
}

