﻿using System;
using System.Net;
using System.IO;
using System.Text;
using Core;

namespace AsvClient
{
	public class AsvHttpConnection: AsvConnection {
		public AsvHttpConnection(IPEndPoint addr): base(addr) {
		}

		protected virtual string Prefix { get {return "http"; } }

		public string GetUrl() {
			return Prefix + "://" + addr.ToString ();
		}
	}

	public class AsvHttpSecureConnection: AsvHttpConnection {
		public AsvHttpSecureConnection(IPEndPoint addr): base(addr) {
		}

		protected override string Prefix { get {return "https"; } }
	}

	public class AsvHttpRequest: IAsvClientRequest {
		private DataSlice ds;

		public AsvHttpRequest (DataSlice ds) {
			this.ds = ds;
		}

		public DataSlice GetContent() {
			return ds;
		}
	}

	public class AsvHttpResponse: IAsvClientResponse {
		DataSlice ds;

		public AsvHttpResponse(DataSlice ds) {
			this.ds = ds;
		}

		public DataSlice GetContent() {
			return ds;
		}		
	}

	public class AsvHttpCient: AsvClient {
		public AsvHttpCient (AsvHttpConnection connection): base(connection) {
		}

		public override IAsvClientResponse Send(IAsvClientRequest request) {
			var httpWebRequest = (HttpWebRequest)WebRequest.Create(((AsvHttpConnection) connection).GetUrl());
			httpWebRequest.ContentType = "application/json";
			httpWebRequest.Method = "POST";
			using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream())) {
				streamWriter.Write(request.GetContent().ToJson());
			}
			var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
			string result = String.Empty;
			using (var streamReader = new StreamReader(httpResponse.GetResponseStream())) {
				result = streamReader.ReadToEnd();
			}
			if (result[0] != '{')
				result = "{\"value\":\"" + result + "\"}";
			return new AsvHttpResponse (DataSlice.FromJson (result));
		}
	}
}

