﻿using System;
using System.Net;
using Core;

namespace AsvClient
{
	public interface IAsvConnection {
	}

	public class AsvConnection: IAsvConnection {
		protected IPEndPoint addr;

		public AsvConnection(IPEndPoint addr) {
			this.addr = addr;
		}
	}

	public interface IAsvClientRequest {
		DataSlice GetContent();
	}

	public interface IAsvClientResponse {
		DataSlice GetContent();
	}

	public interface IAsvClient {
		IAsvClientResponse Send (IAsvClientRequest request);
		//TODO
		//void AsyncSend(IAsvClientRequest request);
		//IAsvClientResponse AsyncRecv();
	}

	public abstract class AsvClient: IAsvClient {
		protected IAsvConnection connection;

		public AsvClient(IAsvConnection connection) {
			this.connection = connection;
		}

		public AsvClient(IPEndPoint addr) {
			connection = new AsvConnection (addr);
		}

		public abstract IAsvClientResponse Send (IAsvClientRequest request);
		//TODO
		//public abstract void AsyncSend(IAsvClientRequest request);
		//public abstract IAsvClientResponse AsyncRecv();
	}

}

