﻿using AsvApi;
using System.Text;

namespace AsvStdLib
{
    public class StdLib : AbstractPlugin {

        private AsvPublicApiMethod test = (inputData, api) => {
            return new byte[] { 73, 116, 32, 119, 111, 114, 107, 115, 33 }; //It works!
        };

		private AsvPublicApiMethod testDs = (inputData, api) => {
			return Encoding.UTF8.GetBytes("{\"testint\": 1, \"teststr\": \"asd\"}");
		};

		public StdLib(IAsvServerApi api): base(api) {
        }

        public override string Description() {
            return "Standart library of public api methods.";
        }

        public override string Name() {
            return "StdLib";
        }

        public override int Run() {
			api.AddPublicApiMethod("test", new AsvMethodInfo(new AsvPublicApiMethodTemplate(test), null));
			api.AddPublicApiMethod("testds", new AsvMethodInfo(new AsvPublicApiMethodTemplate(testDs), null));
            return 0;
        }
    }
}
