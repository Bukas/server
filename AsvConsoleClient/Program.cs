﻿using System;
using System.Net;
using AsvClient;
using Core;

namespace AsvConsoleClient
{
	class MainClass {
		public static void Main (string[] args) {
			var c = new AsvHttpCient (new AsvHttpConnection (new IPEndPoint (new IPAddress (new byte[] { 127, 0, 0, 1 }), 12853)));
			var ds = c.Send (new AsvHttpRequest (DataSlice.FromJson ("{\"method\": \"testds\", \"version\": 1.0}"))).GetContent();
			Console.WriteLine (ds.ToJson ());
			Console.ReadKey ();
		}
	}
}
