﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using Core;
using DataScheme;

namespace DataStructures
{
	public class Link
	{
		public LinkType Type { get; set; }
	}

	public class EntityAttribute
	{
		private int changed = 0;

		public int Changed { get { return changed; } }

		public Object Value { get; set; }
	}

	public class EntityRecord
	{
		private Dictionary<String, EntityAttribute> attrs = new Dictionary<String, EntityAttribute>();
		private Dictionary<KeyValuePair<String, DateTime>, EntityAttribute> histAttrs = new Dictionary<KeyValuePair<String, DateTime>, EntityAttribute>();
		private int changed = 0;

		public int Changed { get { return changed; } }

		public EntityAttribute Attr(string AttrName)
		{
			return attrs [AttrName];
		}

		public EntityAttribute HistAttr(string AttrName, DateTime dt)
		{
			return histAttrs[new KeyValuePair<String, DateTime>(AttrName, dt)];
		}
	}

	public class Entity
	{
		private class EntityEnumerator: IEnumerator
		{
			private int pos = -1;
			private List<EntityRecord> recs;

			public EntityEnumerator(List<EntityRecord> r)
			{
				recs = r;
			}

			public bool MoveNext()
			{
				pos++;
				return pos < recs.Count;
			}

			public void Reset()
			{
				pos = -1;
			}

			public object Current
			{
				get
				{
					try
					{
						return recs[pos];
					}
					catch(IndexOutOfRangeException) 
					{
						throw new InvalidOperationException ();
					}
				}
			}
		}

		private Dictionary<String, Entity> refs = new Dictionary<String, Entity>();
		private List<EntityRecord> recs = new List<EntityRecord> ();

		public EntityType Type { get; private set; }
		public Entity Parent { get; private set; }
		public string EntityName { get; private set; }

		public bool Paging { get; set; }
		public int PageCount { get; set; }

		public Entity(EntityType type, Entity parent, string name)
		{
			Type = type;
			Parent = parent;
			EntityName = name;
		}

		public Entity Ref(string RefName)
		{
			return refs [RefName];
		}

		public Link LinkTo(string RefName)
		{
			return null;
		}

		public EntityAttribute HistAttr(string AttrName, DateTime dt)
		{
			if (recs.Count > 0)
				return recs [0].HistAttr (AttrName, dt);
			else
				return null;
		}
	}

}

